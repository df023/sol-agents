export function formatPhone(phone) {
  return phone.replace(/^(\d{2})(\d{2})(\d{3})(\d{4})$/, "+$1 $2 $3 $4");
}

export function capitalize(string) {
  return string[0].toUpperCase() + string.slice(1);
}

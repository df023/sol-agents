module.exports = {
  purge: ["./src/**/*.js"],
  theme: {
    colors: {
      white: "#FFFFFF",
      gray: {
        100: "#979797",
        200: "#818C99"
      },
      black: "#021318",
      yellow: "#FFF700",
      red: "#E64646",
      green: {
        100: "#4BB34B",
        200: "#123A47",
        300: "#0A2027"
      }
    },
    fontSize: {
      10: "1rem",
      12: "1.2rem",
      14: "1.4rem",
      16: "1.6rem",
      18: "1.8rem",
      24: "2.4rem",
      36: "3.6rem",
      66: "6.6rem",
      96: "9.6rem"
    },
    spacing: {
      0: "0",
      6: "0.6rem",
      7: "0.7rem",
      8: "0.8rem",
      10: "1rem",
      12: "1.2rem",
      13: "1.3rem",
      15: "1.5rem",
      16: "1.6rem",
      17: "1.7rem",
      18: "1.8rem",
      20: "2rem",
      24: "2.4rem",
      30: "3rem",
      32: "3.2rem",
      34: "3.4rem",
      35: "3.5rem",
      44: "4.4rem",
      56: "5.6rem",
      58: "5.8rem",
      60: "6rem",
      63: "6.3rem",
      68: "6.8rem",
      80: "8rem",
      115: "11.5rem",
      120: "12rem"
    },
    maxHeight: {
      618: "61.8rem",
      608: "60.8rem",
      800: "80rem",
      900: "90rem"
    },
    maxWidth: {
      320: "32rem",
      420: "42rem",
      610: "61rem",
      1096: "109.6rem"
    },
    boxShadow: {
      default: "0px 1px 20px rgba(0, 0, 0, 0.25)",
      lg: "0px 4px 4px rgba(0, 0, 0, 0.25)"
    },
    extend: {
      screens: {
        md: "810px",
        sm: "499px",
        xs: "420px",
        iphone5gg: { max: "320px" }
      },
      letterSpacing: {
        wide: "0.03rem"
      },
      lineHeight: {
        tight: "1.2",
        normal: "1.44"
      },
      borderRadius: {
        default: "1rem",
        lg: "2rem"
      }
    }
  },
  variants: {},
  plugins: []
};
